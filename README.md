# server-app-template

## Introduction ##

This is a C# Socket Template multi-client created by me (luchoxyz97).
With this template, you can worry only with your own app functionality instead of the socket implementation.

## Apologies ##
Sorry for the commits in spanish, I'm from Argentina and I forget to comment my commits in english.

## Disclaimer ##
I won't take any liability about the use and modification purposes from this source code.

## Leave me a feedback ##
I'll gratefully thank you if you give me a feedback about this project. It helps me to improve my knows about programming.

Thank you.
