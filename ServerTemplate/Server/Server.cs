﻿using System.Net.Sockets;
using System.Net;
using ServerTemplate.Models;
using System.Text.Json;

namespace ServerTesting.Server
{
    public class Server
    {

        private Socket _Socket { get; set; }
        private List<Task> ActiveConnections { get; set; }

        public bool LoadConfig(out Config config)
        {
            try
            {            
                //Read config file
                string configContent = File.ReadAllText(".\\config.json");
                //Load config
                config = JsonSerializer.Deserialize<Config>(configContent);
                return true;
            }
            catch (Exception ex)
            {
                config = null;
                return false;
            }
        }
        public Server()
        {
            if (LoadConfig(out Config config))
            {
                ActiveConnections = new List<Task>();

                IPEndPoint address = new IPEndPoint(IPAddress.Any, config.Port);
                _Socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                _Socket.Bind(address);
                _Socket.Listen();

                Console.WriteLine("Server is running at port " + config.Port + ". Hit ENTER key to exit.");
                ProcessSocket();
            } else
            {
                Console.WriteLine("An error has ocurred trying to initialize server: Config could not be readed.\n" +
                                  "Check config.json file exists and could be read before trying to execute again this server.");
                return;
            }

        }

        private void ProcessSocket()
        {
            _Socket.BeginAccept(OnConnectionRequest, null);

            //This is the 'secret' to await without server app closes
            Console.ReadLine();

        }

        private void OnConnectionRequest(IAsyncResult ar)
        {
            try
            {
                Socket s = _Socket.EndAccept(ar);
                Console.WriteLine("** Client IP " + s.RemoteEndPoint.ToString() + " connected **");

                ActiveConnections.Add(new Task(delegate { new Connection(s.SafeHandle); }));
                ActiveConnections.Last().Start();
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Infinite loop without using while(true)
            _Socket.BeginAccept(OnConnectionRequest, null);
        }


    }
}
