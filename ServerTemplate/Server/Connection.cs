﻿using ServerTesting.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace ServerTesting.Server
{
    public class Connection : Socket
    {
        public Connection(SafeSocketHandle handle) : base(handle) => new Application(this).Run();


        /// <summary>
        /// Returns if connection is alive or not
        /// </summary>
        /// <returns>Connection status</returns>
        public bool IsAlive()
        {
            try
            {
                return !(Poll(1, SelectMode.SelectRead) && Available == 0);
            }
            catch (SocketException) { return false; }
        }


        /// <summary>
        /// Prompts a input to client
        /// </summary>
        /// <returns>Text input by user</returns>
        public string GetMessage()
        {
            try
            {
                byte[] buffer = new byte[4096];
                int bytesRead = Receive(buffer, SocketFlags.None);
                string message = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                return message;
            } catch
            {
                return "";
            }
        }

        /// <summary>
        /// Prints a text on client screen.
        /// </summary>
        /// <param name="message">Message to print</param>
        public void Print(string message)
        { 
            try { Send(Encoding.UTF8.GetBytes(message)); }
            catch { }
        }

        /// <summary>
        /// Prints a message and waits until the user input.
        /// </summary>
        /// <param name="message">Message to print</param>
        /// <returns>User input</returns>
        public string Prompt(string message)
        {
            Print(message);
            return GetMessage();
        }
    }
}
