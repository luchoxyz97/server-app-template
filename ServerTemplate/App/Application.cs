﻿using ServerTesting.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerTesting.App
{
    public class Application
    {
        private Connection Connection { get; set; }

        public Application(Connection connection) {
            this.Connection = connection;
            this.Run();
        }

        private int? GetNumber()
        {
            int? number = null;
            while (number == null)
            {
                try
                {
                    number = Int32.Parse(this.Connection.Prompt("\rEnter number: "));
                    this.Connection.GetMessage();
                }
                catch (FormatException e)
                {
                    number = null;
                }
            }

            return number;

        }
        public void Run()
        {
            /* WRITE THE REMOTE APPLICATION HERE
             * I HAVE CREATED A SIMPLE CALCULATOR, WHICH SUMS 2 NUMBERS, BUT YOU HAVE TO WRITE YOUR OWN LOGIC!
             */
            bool continueCalc = true;

            while (continueCalc)
            {
                int? firstNumber = null;
                int? secondNumber = null;
                int? final = 0;

                this.Connection.Print("Welcome to my example network calculator!!\n");
                firstNumber = GetNumber();
                secondNumber = GetNumber();

                final = firstNumber + secondNumber;
                this.Connection.Print(String.Format("\n\rThe result is {0}\n", final));
                this.Connection.Print("\rDo you want to do another sum <Y/N>?");
                continueCalc = this.Connection.GetMessage().ToUpper() == "Y";
            }

            this.Connection.Print("\nOk, goodbye!");
            this.Connection.Close();
        }

    }
}
